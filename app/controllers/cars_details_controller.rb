class CarsDetailsController < ApplicationController

  	def index
	  	if params[:url].present?
		  	uri = URI.parse(params[:url])
		  	base_url = "#{uri.scheme}://#{uri.host}"
		  	complete_url = if uri.path.split("/").last.to_i == 0 then base_url+uri.path+"/"+Time.now.year.to_s else base_url+uri.path end
		  	redis_array = complete_url.split("/")
		  	redis_array.pop
		  	redis_key = redis_array.join("/")
  		  	@scrapped_urls = ["#{complete_url}"]
	  		@array = []
	  		if "#{uri.scheme}://#{uri.host}/#{uri.path.split("/").second}" == "http://uae.yallamotor.com/new-cars" and uri.path.split("/").third.present?
			  	if $redis.get("#{redis_key}").present?
			  		@redis_array = JSON.parse($redis.get("#{redis_key}"))
			  		@page = HTTParty.get(complete_url)
	  				page_parse = Nokogiri::HTML(@page)
				  	page_parse.css('#accordion2 .panel-default').each do |accordion|
					  	if accordion.css('a').first['href'] != "javascript:\;" 
				  	 		@scrapped_urls << if (base_url+accordion.css('a').first['href']).split("/").last.to_i == 0 then "#{base_url+accordion.css('a').first['href']}/#{Time.now.year}" else base_url+accordion.css('a').first['href'] end
						end
				 	end
				  	@array = @redis_array.select {|element| element.first.first == complete_url}
				  	@array << @redis_array.reject {|element| element.first.first == complete_url}
				  	@array.flatten!
			  	else	
			  		@page = HTTParty.get(complete_url)
		  			if @page.request.last_uri.to_s == complete_url
		  				page_parse = Nokogiri::HTML(@page)
					  	page_parse.css('#accordion2 .panel-default').map { |accordion| if accordion.css('a').first['href'] != "javascript:\;" then @scrapped_urls << base_url+accordion.css('a').first['href'] else nil end}
					  	@scrapped_urls.each_with_index do |url,url_index|
						  	if url_index > 0
							  	@page = HTTParty.get(url)
							  	page_parse = Nokogiri::HTML(@page)
						  	end
							content = page_parse.css('#collapse1')
							if content.present?
								@array << {"#{url}" => []}
						  		@array[url_index]["#{url}"] << Parallel.map(content.css('.new-car-name'), in_processes: 20) do |car,car_index|
									array_making(car,base_url)
								end
								@array[url_index]["#{url}"].flatten!
							end
						end
						$redis.set("#{redis_key}", @array.to_json)
					else
						@notice = "The provided url was redirected by app. Make sure your path is correct."
			  		end
				end
			else
				@notice = "This app is not made to scrape this url"
			end
	  	end
	end

private
	def array_making(car,base_url)
		car_hash = {"model" => car.text, "versions" => []}
  		car_details = HTTParty.get(base_url+car['href'].to_s)
  		car_details_page = Nokogiri::HTML(car_details)
		car_details_page.css('.prices-and-specs .new-cars-results-text h4').each_with_index do |car_details,details_index|
			car_version_hash = {"car_version" => car_details.css("a").text.gsub(/([\n])/, ''), "price" => car_details.css("span").text}
			car_hash["versions"] << car_version_hash
		end
		return car_hash
	end
end