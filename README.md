* #Quick summary
- This repository was made in order to manage my code for a technical assignment as part of my interview.


* ##Summary of set up
- bundle install
- rake db:create
- rake db:migrate
- Needs "redis" installed in your system(for caching web urls).

* ##Dependencies
- Rails: rails 4.2.5
- Ruby: ruby 2.2.1p85

* ##Database configuration
- Database: Postgresql

* ##Assumptions:
- Internet connection must be stable for fetching the url provided.
- Redis installed in your system.
- The pages of target site remain static.
- No dynamic data is being pulled in by ajax.
- The structure of page's being parsed remain unchanged e-g classes and ids of elements.
-